/*
 * hello.c
 *
 *  Created on: 19 May 2017
 *      Author: gokhanettin
 */


#include "hello.h"
#include <stdio.h>

void hello_world(void)
{
	printf("Hello World!\n");
}
