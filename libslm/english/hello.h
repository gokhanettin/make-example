/*
 * hello.h
 *
 *  Created on: 19 May 2017
 *      Author: gokhanettin
 */

#ifndef HELLO_H_
#define HELLO_H_

#ifdef BUILDING_HELLO_DLL
#define HELLO_DLL __declspec(dllexport)
#else
#define HELLO_DLL __declspec(dllimport)
#endif

void HELLO_DLL hello_world(void);

#endif /* HELLO_H_ */
