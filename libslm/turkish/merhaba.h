/*
 * merhaba.h
 *
 *  Created on: 19 May 2017
 *      Author: gokhanettin
 */


#ifndef MERHABA_H_
#define MERHABA_H_

#ifdef BUILDING_HELLO_DLL
#define HELLO_DLL __declspec(dllexport)
#else
#define HELLO_DLL __declspec(dllimport)
#endif

void __stdcall HELLO_DLL hello_world_turkish(void);

#endif /* MERHABA_H_ */
