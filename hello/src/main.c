/*
 * main.c
 *
 *  Created on: 19 May 2017
 *      Author: gokhanettin
 */

/* Both includes should work */
#include "func1.h"
#include "func2/func2.h"
#include <hello.h>
#include <merhaba.h>

int main(void)
{
	func1();
	func2();
	hello_world();
	hello_world_turkish();
	return 0;
}
